#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys, logging, re, heapq, operator
from collections import deque, Counter
from math import log

# logging.basicConfig(stream=sys.stderr, level=logging.DEBUG)



class NBModel:


    def __init__(self, params):

        # just training data comes around here
        pos_path_list = deque(params['pos'])
        neg_path_list = deque(params['neg'])

        # marginal probability of the classes
        pos_count = len(params['pos'])
        neg_count = len(params['neg'])
        self.pos_marginal = pos_count / (pos_count + neg_count)
        self.neg_marginal = neg_count / (pos_count + neg_count)

        self.pos_vocabulary = Counter()
        self.neg_vocabulary = Counter()

        while pos_path_list or neg_path_list:
            try:
                with open(pos_path_list.popleft(), 'r') as f:
                    file = f.read()
                    #words = re.split(r'\W+', file) # breaks aphostrophes
                    #print(list(filter(lambda x: x != None, re.split(r'<br />+|(\?)+|(!)+|:[^)(]|[\s.,();]', file))) # empty words
                    words = filter(lambda x: x != None and x != "", re.split(r'`|/|\'|"|--|<br />+|(\?)+|(!)+|:[^)(]|[\s.,();]', file)) # empty words
                    for w in words:
                        # self.pos_vocabulary[w.lower()] += 1
                        self.pos_vocabulary[w] += 1
                with open(neg_path_list.popleft(), 'r') as f:
                    file = f.read()
                    words = filter(lambda x: x != None and x != "", re.split(r'`|/|\'|"|--|<br />+|(\?)+|(!)+|:[^)(]|[\s.,();]', file)) # empty words
                    for w in words:
                        # self.neg_vocabulary[w.lower()] += 1
                        self.neg_vocabulary[w] += 1
            except IndexError:
                pass
        
        logging.debug('\n## POSITIVE Reviews stats ##')
        logging.debug('5 MOST COMMON words:      '+str(self.pos_vocabulary.most_common(5)))
        logging.debug('5 LEAST COMMON words:     '+str(self.pos_vocabulary.most_common()[:-5-1:-1]))
        pos_distinct = len(self.pos_vocabulary.keys())
        logging.debug('Number of DISTINCT WORDS: '+str(pos_distinct))
        logging.debug('## NEGATIVE Reviews stats ##')
        logging.debug('5 MOST COMMON words:      '+str(Counter(self.neg_vocabulary).most_common(5)))
        logging.debug('5 LEAST COMMON words:     '+str(self.neg_vocabulary.most_common()[:-5-1:-1]))
        neg_distinct = len(self.neg_vocabulary.keys())
        logging.debug('Number of DISTINCT WORDS: '+str(neg_distinct))
        logging.debug('## VOCABULARY stats ##')
        voc_distinct = pos_distinct + neg_distinct
        logging.debug('Number of DISTINCT WORDS: '+str(voc_distinct))
        logging.debug('(Information not normalized by review length, case-sensitive)\n')

        # number of distinct word positions in all members of a class
        n = {}
        n['pos'] = sum(self.pos_vocabulary.values())
        n['neg'] = sum(self.neg_vocabulary.values())
        

        self.naive_prob = {}
        self.naive_prob['pos'] = {}
        self.naive_prob['neg'] = {}
        self.naive_prob['third_ugly_thing'] = {} # can't remember what i would put here. the difference between pos and neg maybe?
        # i was trying to sort by this difference?
        # just for curiosity


        # naive probability for each word
        for w in (set(self.pos_vocabulary)|set(self.neg_vocabulary)):
            self.naive_prob['pos'][w] = log((self.pos_vocabulary[w] + 1) /(n['pos'] + voc_distinct))
            self.naive_prob['neg'][w] = log((self.neg_vocabulary[w] + 1) /(n['neg'] + voc_distinct))
            self.naive_prob['third_ugly_thing'][w] = (self.naive_prob['pos'][w] - self.naive_prob['neg'][w]) ** 2
            #print(w + '\t\t' + str(naive_prob['pos'][w]) + '\t' + str(naive_prob['neg'][w]) + '\t' + str(naive_prob['third_ugly_thing'][w]))

        #for key, val in sorted(naive_prob['third_ugly_thing'].items(), key=operator.itemgetter(1)):
            #print (key, val, 'POS' if naive_prob['pos'][key] > naive_prob['neg'][key]  else 'NEG')


    #def train():

    def get_matrix(self, params) :
        # params is a dictionary with pos and neg files never seen

        pos_path_list = deque(params['pos'])
        neg_path_list = deque(params['neg'])
        pos_count = len(params['pos'])
        neg_count = len(params['neg'])

        vp = 0
        fp = 0
        vn = 0
        fn = 0

        while pos_path_list:
            try:
                with open(pos_path_list.popleft(), 'r') as f:
                    file = f.read()
                    #words = re.split(r'\W+', file) # breaks aphostrophes
                    #print(list(filter(lambda x: x != None, re.split(r'<br />+|(\?)+|(!)+|:[^)(]|[\s.,();]', file))) # empty words
                    words = filter(lambda x: x != None and x != "", re.split(r'`|/|\'|"|--|<br />+|(\?)+|(!)+|:[^)(]|[\s.,();]', file)) # empty words
                    hpos = 0
                    hneg = 0
                    for w in words:
                        hpos += self.naive_prob['pos'].get(w, 0) # default value 1 if non-existent word ;)
                        hneg += self.naive_prob['neg'].get(w, 0)
                    if ( hpos > hneg ):
                        vp += 1
                    else: fn += 1
            except IndexError:
                pass


        while neg_path_list:
            try:
                with open(neg_path_list.popleft(), 'r') as f:
                    file = f.read()
                    words = filter(lambda x: x != None and x != "", re.split(r'`|/|\'|"|--|<br />+|(\?)+|(!)+|:[^)(]|[\s.,();]', file)) # empty words
                    hpos = 0
                    hneg = 0
                    for w in words:
                        hpos += self.naive_prob['pos'].get(w, 0)
                        hneg += self.naive_prob['neg'].get(w, 0)
                    if (hpos > hneg) :
                        fp += 1
                    else: vn += 1
            except IndexError:
                pass

        return {'vp': vp, 'fp': fp, 'vn': vn, 'fn': fn}