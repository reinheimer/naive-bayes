#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys, getopt, os, logging, re
from TrainingSet import NBModel
from collections import Counter

def print_usage() :
    print('Usage: ', sys.argv[0], '-p<positives_folder> -n<negatives_folder> -o<output_file> [-v]')

def print_man() :
    print('\n')
    print('Long version of the usage manual\n\n')
    print('\t-c\t\tshow Credits')
    print('\n')
    print('TRAINING')
    print('\t-f<k>\tk-Fold cross validation (default 10)')
    print('\t-n<path>\tpath to Negative instances (default ./IMDB/pos)')
    print('\t-p<path>\tpath to Positive instances (default ./IMDB/neg)')
    print('\t-s<index>\tStarting index of training set (default 14)')
    print('\t-t<size>\tTotal size of training set (default 24000)')
    print('\n')
    print('REPORTING')
    print('\t-o<file>\tfile for Output information')
    print('\t-v\t\tenable Verbose')

def parse_cmd(argv) :
    try:
        opts, args = getopt.getopt(sys.argv[1:], "hc:p:n:s:t:o:v", ["help", "output="])
    except getopt.GetoptError as err:
        print(err) # will print something like "option -a not recognized"
        print_usage()
        sys.exit(2)

    #default values
    output = None
    verbose = False
    positives_path = 'IMDB/pos/'
    negatives_path = 'IMDB/neg/'
    start_index = 14
    total_instances = 24000 #24000
    cv_folds = 10

    for o, a in opts:
        if o == "-v":
            verbose = True
            logging.basicConfig(stream=sys.stderr, level=logging.DEBUG)
        elif o in ("-h", "--help"):
            print_man()
            sys.exit()
        elif o in ("-o", "--output"):
            output = a
        elif o == "-p":
            positives_path = a
        elif o == "-n":
            negatives_path = a
        elif o == "-s":
            start_index = a
        elif o == "-t":
            total_instances = a
        elif o == "-c":
            cv_folds = a
        else:
            assert False, "unhandled option"

    # watch for the tabs
    return {'verbose': verbose,
            'output': output,
            'positives_path': positives_path,
            'negatives_path': negatives_path,
            'start_index': start_index,
            'total_instances': total_instances,
            'cv_folds': cv_folds}

# Expected result:
#   a list where each index contains the file names for cross-validating
#   (training and testing) one of the models generated.
#   e.g.: [{training:{pos:[1, 2, 3, 4], neg:[1, 2, 3, 4]}, test:{pos: [5], neg: [5]]},
#          {training:{pos:[2, 3, 4, 5], neg:[2, 3, 4, 5]}, test:{pos: [1], neg: [1]]},
#          etc... ]
def group_instances(groups, chunk_size, params) :

    # para ficar bem auto-contido, pegar direto os paths e os índices (+ 4 parâmetros)

    # list.append

    i_size = int(chunk_size)
    s_index = int(params['start_index'])
    i_total = int(params['total_instances'])
    result = []

    positive_fnames = [x for x in sorted(os.listdir(params['positives_path']), key=lambda x: (int(re.sub('\D','',x)),x)) if os.path.isfile(params['positives_path']+x)]
    if len(positive_fnames) < i_total + s_index:
        print('There are not enough positive files for your experiment.')
        sys.exit()

    negative_fnames = [x for x in sorted(os.listdir(params['negatives_path']), key=lambda x: (int(re.sub('\D','',x)),x)) if os.path.isfile(params['negatives_path']+x)]
    if len(negative_fnames) < i_total + s_index:
        print('There are not enough negative files for your experiment.')
        sys.exit()

    # number of rotations defined by <groups>
    for k in range(1, groups + 1):
        files_set = {}

        files_set['train'] = {}
        files_set['train']['pos'] = []
        files_set['train']['neg'] = []

        files_set['test'] = {}
        files_set['test']['pos'] = []
        files_set['test']['neg'] = []

        # training
        for i in range(1, groups + 1):
            if i != k:
                for j in range(0, i_size):
                    file_index = (i - 1) * i_size + j
                    files_set['train']['pos'].append(params['positives_path'] + positive_fnames[file_index + s_index])
                    files_set['train']['neg'].append(params['negatives_path'] + negative_fnames[file_index + s_index])

        # testing
        for j in range(0, i_size):
            file_index = (k - 1) * i_size + j
            files_set['test']['pos'].append(params['positives_path'] + positive_fnames[file_index + s_index])
            files_set['test']['neg'].append(params['negatives_path'] + negative_fnames[file_index + s_index])

        #print(str(files_set['train']['pos']) + "\n")
        #print(str(files_set['test']['pos']) + "\n\n")

        result.append(files_set)

    return result


def main () :

    accum = {}
    accum['vp'] = 0
    accum['vn'] = 0
    accum['fp'] = 0
    accum['fn'] = 0

    params = parse_cmd(sys.argv)
    cv_folds = params['cv_folds']
    cv_subgroup_s = params['total_instances'] / cv_folds

    cv_groups = group_instances(cv_folds, cv_subgroup_s, params)

    for i in range(0, 10):
        e = NBModel(cv_groups[i]['train'])
        matrix_i = e.get_matrix(cv_groups[i]['test'])
        logging.debug('Wk\tP(wk|Pos)\tP(wk|Neg)')
        print(matrix_i)
        accum['vp'] += matrix_i['vp']
        accum['vn'] += matrix_i['vn']
        accum['fp'] += matrix_i['fp']
        accum['fn'] += matrix_i['fn']

    avg_vp = accum['vp'] / cv_folds
    avg_vn = accum['vn'] / cv_folds
    avg_fp = accum['fp'] / cv_folds
    avg_fn = accum['fn'] / cv_folds

    ninechar_vp = "{:9.2f}".format(avg_vp)
    ninechar_fn = "{:9.2f}".format(avg_fn)
    ninechar_fp = "{:9.2f}".format(avg_fp)
    ninechar_vn = "{:9.2f}".format(avg_vn)

    print('\nAVERAGE CONFUSION MATRIX')

    print('       ' + ' | ' + 'Predicted' + ' | ' + 'Predicted')
    print('       ' + ' | ' + '   YES   ' + ' | ' + '    NO   ')
    print('-----------------------------------------')
    print(' Actual' + ' | ' + ninechar_vp + ' | ' + ninechar_fn)
    print('  YES  ' + ' | ' + '         ' + ' | ' + '         ')
    print('-----------------------------------------')
    print(' Actual' + ' | ' + ninechar_fp + ' | ' + ninechar_vn)
    print('   NO  ' + ' | ' + '         ' + ' | ' + '         ')
    print('-----------------------------------------')

    print('\nPERFORMANCE MEASURES')

    # Precision measure: p = (VP + VN)/(P + N)
    p = (avg_vp + avg_vn) / (2 * cv_subgroup_s)
    fmt_p = "{:9.2f}".format(p)
    print('Precision:\t\t' + fmt_p)

    # Recall measure: r = VP / (VP + FN)
    r = avg_vp / (avg_vp + avg_fn)
    fmt_r = "{:9.2f}".format(r)
    print('True Positives Rate:\t' + fmt_r)

    # False positives rate: FP / (FP + VN)
    fmt_fpr = "{:9.2f}".format(avg_fp / (avg_fp + avg_vn))
    print('False Positives Rate:\t' + fmt_fpr)

    # F-measure: 2rp / (r + p)
    fmt_fms = "{:9.2f}".format(2 * r * p / (r + p))
    print('F-Measure:\t\t' + fmt_fms)


if __name__ == "__main__":
    main()
